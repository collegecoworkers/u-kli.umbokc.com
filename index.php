<!DOCTYPE html>
<html lang="ru" ea>
<head>
	<meta charset="UTF-8">
	<title>Фоторедактор</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="http://cdn.uwebu.ru/u-emmet-attr/src/ea.css?v=1.2">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.css">
	<link rel="stylesheet" href="css/app.css">
</head>
<body>

	<header class="header">
		<span class="title">Фоторедактор</span>
		<div class="navbar">
			<nav class="nav">
				<a download="image.png" data-action=download title="Download" class="nav-btn nav-btn--success">
					<span class="fa fa-download"></span>
				</a>
			</nav>
		</div>
	</header>

	<div class="editor">
		<div class="toolbar">
			<button class="toolbar-button" action='text' title=""><span class="fa fa-font"></span></button>
			<button class="toolbar-button" action='filter' title=""><span class="fa fa-filter"></span></button>
			<button class="toolbar-button" action='image_upload' title="Загрузить картинку" pos:r cur:p>
				<input pos:a d:b w:100p h:100p t:0 l:0 cur:p op#0 type="file" id="file">
				<span class="fa fa-image"></span>
			</button>
		</div>
	</div>

	<canvas id="app" width="500" height="500"></canvas>

	<div class="filters">
		<div class="controls">
			<h3>Фильтры:</h3>
			<label>Использовать WebGl <input type="checkbox" id="webgl" style="margin-left: 1px;" checked=""></label>
			<div id="bench"></div>
			<div class='the-f'>
				<label><span>Grayscale:</span> <input type="checkbox" filter-id="grayscale"></label>
				<div filter-op='grayscale'>
					<label m:t><span>Avg.</span> <input type="radio" filter-id="average" name="grayscale"></label>
					<label><span>Lum.</span> <input type="radio" filter-id="lightness" name="grayscale"></label>
					<label><span>Light.</span> <input type="radio" filter-id="luminosity" name="grayscale"></label>
				</div>
			</div>
			<div class='the-f'>
				<label><span>Invert:</span> <input type="checkbox" filter-id="invert"></label>
			</div>
			<div class='the-f'>
				<label><span>Sepia:</span> <input type="checkbox" filter-id="sepia"></label>
			</div>
			<div class='the-f'>
				<label><span>Black/White:</span> <input type="checkbox" filter-id="blackwhite"></label>
			</div>
			<div class='the-f'>
				<label><span>Brownie:</span> <input type="checkbox" filter-id="brownie"></label>
			</div>
			<div class='the-f'>
				<label><span>Vintage:</span> <input type="checkbox" filter-id="vintage"></label>
			</div>
			<div class='the-f'>
				<label><span>Kodachrome:</span> <input type="checkbox" filter-id="kodachrome"></label>
			</div>
			<div class='the-f'>
				<label><span>Technicolor:</span> <input type="checkbox" filter-id="technicolor"></label>
			</div>
			<div class='the-f'>
				<label><span>Polaroid:</span> <input type="checkbox" filter-id="polaroid"></label>
			</div>
			<div class='the-f'>
				<label><span>Remove color:</span> <input type="checkbox" filter-id="remove-color"></label>
				<div filter-op='remove-color'>
					<label>Color: <input type="color" filter-id="remove-color-color" value="#00f900"></label>
					<label>Distance: <input type="range" filter-id="remove-color-distance" value="0.02" min="0" max="1" step="0.01"></label>
				</div>
			</div>
			<div class='the-f'>
				<label><span>Brightness:</span> <input type="checkbox" filter-id="brightness"></label>
				<div filter-op='brightness'>
					<label>Value: <input type="range" filter-id="brightness-value" value="0.1" min="-1" max="1" step="0.003921"></label>
				</div>
			</div>
			<div class='the-f'>
				<label><span>Gamma:</span> <input type="checkbox" filter-id="gamma"></label>
				<div filter-op='gamma'>
					<label>Red: <input type="range" filter-id="gamma-red" value="1" min="0.2" max="2.2" step="0.003921"></label>
					<label>Green: <input type="range" filter-id="gamma-green" value="1" min="0.2" max="2.2" step="0.003921"></label>
					<label>Blue: <input type="range" filter-id="gamma-blue" value="1" min="0.2" max="2.2" step="0.003921"></label>
				</div>
			</div>
			<div class='the-f'>
				<label><span>Contrast:</span> <input type="checkbox" filter-id="contrast"></label>
				<div filter-op='contrast'>
					<label>Value: <input type="range" filter-id="contrast-value" value="0" min="-1" max="1" step="0.003921"></label>
				</div>
			</div>
			<div class='the-f'>
				<label><span>Saturation:</span> <input type="checkbox" filter-id="saturation"></label>
				<div filter-op='saturation'>
					<label>Value: <input type="range" filter-id="saturation-value" value="0" min="-1" max="1" step="0.003921"></label>
				</div>
			</div>
			<div class='the-f'>
				<label><span>Hue:</span> <input type="checkbox" filter-id="hue"></label>
				<div filter-op='hue'>
					<label>Value: <input type="range" filter-id="hue-value" value="0" min="-2" max="2" step="0.002"></label>
				</div>
			</div>
			<div class='the-f'>
				<label><span>Noise:</span> <input type="checkbox" filter-id="noise"></label>
				<div filter-op='noise'>
					<label>Value: <input type="range" filter-id="noise-value" value="100" min="0" max="1000"></label>
				</div>
			</div>
			<div class='the-f'>
				<label><span>Pixelate</span> <input type="checkbox" filter-id="pixelate"></label>
				<div filter-op='pixelate'>
					<label>Value: <input type="range" filter-id="pixelate-value" value="4" min="2" max="20"></label>
				</div>
			</div>
			<div class='the-f'>
				<label><span>Blur:</span> <input type="checkbox" filter-id="blur"></label>
				<div filter-op='blur'>
					<label>Value: <input type="range" filter-id="blur-value" value="0.1" min="0" max="1" step="0.01"></label>
				</div>
			</div>
			<div class='the-f'>
				<label><span>Sharpen:</span> <input type="checkbox" filter-id="sharpen"></label>
			</div>
			<div class='the-f'>
				<label><span>Emboss:</span> <input type="checkbox" filter-id="emboss"></label>
			</div>
			<div class='the-f'>
				<label><span>Blend Color:</span> <input type="checkbox" filter-id="blend"></label>
				<div filter-op='blend'>
					<label>
						Mode:
						<select filter-id="blend-mode" name="blend-mode">
							<option selected="" value="add">Add</option>
							<option value="diff">Diff</option>
							<option value="subtract">Subtract</option>
							<option value="multiply">Multiply</option>
							<option value="screen">Screen</option>
							<option value="lighten">Lighten</option>
							<option value="darken">Darken</option>
							<option value="overlay">Overlay</option>
							<option value="exclusion">Exclusion</option>
							<option value="tint">Tint</option>
						</select>
					</label>
					<label>Color: <input type="color" filter-id="blend-color" value="#00f900"></label>
					<label>Alpha: <input type="range" filter-id="blend-alpha" min="0" max="1" value="1" step="0.01"></label>
				</div>
			</div>
		</div>
	</div>

	<div class="text-options">
		<h3>Свойства текста:</h3>
		<div id="text-controls">
			<div class='the-f'>
				<label>Color:</label>
				<input type="color" id="text-color" size="10">
			</div>
			<div class='the-f'>
				<label for="font-family" style="display:inline-block">Font family:</label>
				<select id="font-family">
					<option value="arial">Arial</option>
					<option value="helvetica" selected>Helvetica</option>
					<option value="myriad pro">Myriad Pro</option>
					<option value="delicious">Delicious</option>
					<option value="verdana">Verdana</option>
					<option value="georgia">Georgia</option>
					<option value="courier">Courier</option>
					<option value="comic sans ms">Comic Sans MS</option>
					<option value="impact">Impact</option>
					<option value="monaco">Monaco</option>
					<option value="optima">Optima</option>
					<option value="hoefler text">Hoefler Text</option>
					<option value="plaster">Plaster</option>
					<option value="engagement">Engagement</option>
				</select>
			</div>
			<div class='the-f'>
				<label for="text-align" style="display:inline-block">Text align:</label>
				<select id="text-align">
					<option value="left">Left</option>
					<option value="center">Center</option>
					<option value="right">Right</option>
					<option value="justify">Justify</option>
				</select>
			</div>
			<div class='the-f'>
				<label for="text-bg-color">Background color:</label>
				<input type="color" id="text-bg-color" size="10">
			</div>
			<div class='the-f'>
				<label for="text-lines-bg-color">Background text color:</label>
				<input type="color" id="text-lines-bg-color" size="10">
			</div>
			<div class='the-f'>
				<label for="text-stroke-color">Stroke color:</label>
				<input type="color" id="text-stroke-color">
			</div>
			<div class='the-f'>
				<label for="text-stroke-width">Stroke width:</label>
				<input type="range" value="1" min="1" max="5" id="text-stroke-width">
			</div>
			<div class='the-f'>
				<label for="text-font-size">Font size:</label>
				<input type="range" min="1" max="120" step="1" id="text-font-size">
			</div>
			<div class='the-f'>
				<label for="text-line-height">Line height:</label>
				<input type="range" min="0" max="10" step="0.1" id="text-line-height">
			</div>
		</div>
		<div id="text-controls-additional">
			<div class='the-f'>
				<label for=text-cmd-bold>Bold
					<input type='checkbox' name='fonttype' id="text-cmd-bold">
				</label>
			</div>

			<div class='the-f'>
				<label for=text-cmd-italic>Italic
					<input type='checkbox' name='fonttype' id="text-cmd-italic">
				</label>
			</div>
		</div>
	</div>

	<div class="init-container">
		<input pos:a d:b w:100p h:100p t:0 l:0 cur:d op#0 type="file" id="init_file">
		<p>Перетаще сюдай файл или нажмите...</p>
	</div>

	<script src="js/jquery.min.js"></script>
	<script src="http://jscolor.com/release/2.0/jscolor-2.0.4/jscolor.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js"></script>
	<script src="js/fabric.min.js"></script>
	<script src="js/fabric.filters.js"></script>
	<script src="js/fabric.text.js"></script>
	<script src="js/app.js"></script>
</body>
</html>