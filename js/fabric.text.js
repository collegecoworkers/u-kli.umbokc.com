function init_text(){
	function set_prop(prop, val){
		App.canvas.getActiveObject().set(prop, val);
		App.canvas.renderAll();
	}

	App.$get_text('text-color').onchange = function() {set_prop("fill", this.value);};
	App.$get_text('text-bg-color').onchange = function() { set_prop('backgroundColor', this.value);};
	App.$get_text('text-lines-bg-color').onchange = function() { set_prop('textBackgroundColor', this.value);};
	App.$get_text('text-stroke-color').onchange = function() { set_prop('stroke', this.value);};
	App.$get_text('text-stroke-width').onchange = function() { set_prop('strokeWidth', this.value);};
	App.$get_text('font-family').onchange = function() { set_prop('fontFamily', this.value);};
	App.$get_text('text-font-size').onchange = function() { set_prop('fontSize', this.value);};
	App.$get_text('text-line-height').onchange = function() { set_prop('lineHeight', this.value);};
	App.$get_text('text-align').onchange = function() { set_prop('textAlign', this.value);};

// wijzig naar button
	radios5 = App.$name("fonttype");
	for(var i = 0, max = radios5.length; i < max; i++) {
		radios5[i].onclick = function() {
			if(App.$get_text(this.id).checked == true) {
				if(this.id == "text-cmd-bold") { set_prop("fontWeight", "bold");}
				if(this.id == "text-cmd-italic") { set_prop("fontStyle", "italic");}
			} else {
				if(this.id == "text-cmd-bold") { set_prop("fontWeight", "");}
				if(this.id == "text-cmd-italic") { set_prop("fontStyle", "");}
			}
		}
	}
}