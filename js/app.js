document.addEventListener('DOMContentLoaded', function(){
	window.App = {
		canvas: null,
		name: null,
		type: null,
		filters_wrap: $('.filters'),
		text_options_wrap: $('.text-options'),

		text_options: {
			'text-color' : 'fill',
			'text-bg-color' : 'backgroundColor',
			'text-lines-bg-color' : 'textBackgroundColor',
			'text-stroke-color' : 'stroke',
			'text-stroke-width' : 'strokeWidth',
			'font-family' : 'fontFamily',
			'text-font-size' : 'fontSize',
			'text-line-height' : 'lineHeight',
			'text-align' : 'textAlign',
		},

		text_options_cmd: {
			'text-cmd-bold': ['fontWeight', 'bold'],
			'text-cmd-italic': ['fontStyle', 'italic'],
		},

		filters_inp: null,
		filters:['grayscale', 'invert', 'remove-color', 'sepia', 'brownie',
						'brightness', 'contrast', 'saturation', 'noise', 'vintage',
						'pixelate', 'blur', 'sharpen', 'emboss', 'technicolor',
						'polaroid', 'blend-color', 'gamma', 'kodachrome',
						'blackwhite', 'blend-image', 'hue', 'resize'],

		started: false,

		$id: function(id){
			// return document.getElementById(id) || {};
			return document.getElementById(id);
		},
		$name: function(name){
			// return document.getElementsByName(name) || [{}];
			return document.getElementsByName(name);
		},
		$get: function(sel){
			return this.$gets(sel)[0];
		},
		get: function(sel){
			return this.$get(sel);
		},
		$gets: function(sel){
			// return document.querySelectorAll(sel) || [{}];
			return document.querySelectorAll(sel);
		},
		gets: function(sel){
			return this.$gets(sel);
		},
		$get_filter: function(f){
			return this.$get('.filters [filter-id='+f+']');
		},
		$get_text: function(t){
			return this.$id(t);
		},

		dbg: function(mes){
			console.log(mes);
			window.dbg = mes;
		},
		calc_size: function(imgWidth, imgHeight){
			var	maxWidth = App.canvas.width,
			maxHeight = App.canvas.height,
			newWidth, newHeight;

			if (imgWidth >= maxWidth || imgHeight >= maxHeight) {
				if (imgWidth > imgHeight) {
					ratio = imgWidth / maxWidth;
					newWidth = maxWidth;
					newHeight = imgHeight / ratio;
				} else {
					ratio = imgHeight / maxHeight;
					newHeight = maxHeight;
					newWidth = imgWidth / ratio;
				}
			} else {
				newHeight = imgHeight;
				newWidth = imgWidth;
			}

			return {
				width: newWidth,
				height: newHeight
			};
		},
		awake: function(){
			var app = this.get('#app');
			app.setAttribute('width', window.innerWidth);
			app.setAttribute('height', (window.innerHeight - this.get('header').offsetHeight));
		},
		init: function(){
			this.awake();
			this.canvas = new fabric.Canvas('app', {
				preserveObjectStacking: true
			});
			this.filters_inp = this.$gets('.filters [filter-id]');
		},
		start: function(){
			if(App.started !== false) return;

			App.started = true;

			$('.toolbar').show();
			$('.navbar nav').show();

			init_filters();
			init_text();

			function select_object(event){
				var obj = App.canvas.getActiveObject();
				if(obj.type == 'image'){
					App.text_options_wrap.hide();
					App.filters_inp.forEach(function(el){ el.disabled = false; })
					for (var i = 0; i < App.filters.length; i++) {
						App.$get_filter(App.filters[i]) && (
							App.$get_filter(App.filters[i]).checked = !!App.canvas.getActiveObject().filters[i]);
					}
				} else if(obj.type == 'i-text') {
					for (key in App.text_options) {
						if (App.text_options.hasOwnProperty(key)) {
							var val = App.text_options[key];
							var item = App.$get_text(key);
							item && (item.value = App.canvas.getActiveObject()[val]);
						}
					}
					for (key in App.text_options_cmd) {
						if (App.text_options_cmd.hasOwnProperty(key)) {
							var val = App.text_options_cmd[key];
							var item = App.$get_text(key);
							item && (item.checked = val[1] == App.canvas.getActiveObject()[val[0]]);
						}
					}
					App.text_options_wrap.show();
					App.filters_wrap.hide();
				} else {
					App.text_options_wrap.hide();
					App.filters_wrap.hide();
				}
			}

			App.canvas.on({
				'object:selected': select_object,
				'selection:updated': select_object,
				'selection:cleared': function() {
					App.text_options_wrap.hide();
					App.filters_wrap.hide();
					App.filters_inp.forEach(function(el){ el.disabled = true; })
				}
			});
		},

		add_obj: function(obj){
			this.canvas.add(obj);
			this.canvas.setActiveObject(obj);
		},
		add_rect: function(){
			var rect = new fabric.Rect({
				left: 100,
				top: 100,
				fill: 'red',
				height: 200,
				width: 200,
				active: true,
			});
			App.add_obj(rect);
		},
		add_text: function () { 
			var txt = new fabric.IText('Tap and Type', { 
				left: 50,
				top: 100,
				fontFamily: 'arial black',
				fill: '#fff',
				fontSize: 50,
				active: true,
			});
			App.add_obj(txt);
			App.text_options_wrap.show();
		},
		save_image: function(download){
			var image = App.canvas.toDataURL(App.type).replace(App.type, 'image/octet-stream');
			download.setAttribute("href", image);
			download.setAttribute("download", App.name);
		},
		load_image_from: function(item, call_the){
			item.onchange = function handleImage(e) {
				var reader = new FileReader(),
				file = e.target.files[0];
				App.name = file.name;
				App.type = file.type;
				reader.onload = function (event){
					var imgObj = new Image();
					imgObj.src = event.target.result;
					imgObj.onload = function () {
						var image = new fabric.Image(imgObj);
						var new_size = App.calc_size(image.getScaledWidth(), image.getScaledHeight());
						image.scaleToWidth(new_size.width);
						image.scaleToHeight(new_size.height);
						image.set({
							// width: new_size.width,
							// height: new_size.height,
							u_name: App.name,
							angle: 0,
							padding: 10,
							cornersize:10,
							active: true,
						});
						App.canvas.centerObject(image);
						App.add_obj(image);
						App.canvas.renderAll();
						call_the && call_the();
					}
				}
				reader.readAsDataURL(file);
			}
		}
	};

	App.init();
	// App.add_rect();
	// App.add_rect();
	App.load_image_from(App.get('#init_file'), function(){
		App.get('.init-container').style.display = 'none';
		App.get('.canvas-container').style.display = 'block';
		App.start();
	});

	App.load_image_from(App.get('#file'));

	$('.toolbar [action]').click(function(e){
		var action = $(this).attr('action');
		switch(action) {
			case 'filter':
			var obj = App.canvas.getActiveObject();
			if(obj.type == 'image'){
				App.text_options_wrap.hide();
				App.filters_wrap.toggle();
			}
			break;
			case 'text':
				App.add_text();
				App.filters_wrap.hide();
				App.text_options_wrap.show();
			break;
		}
	});

	App.get('.navbar [data-action=download]').onclick = function(e){
		App.save_image(this);
	}

	$('[type=color]').colorpicker();

});