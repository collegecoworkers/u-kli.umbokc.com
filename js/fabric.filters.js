function init_filters() {
	// manually initialize 2 filter backend to give ability to switch:
	var webglBackend = new fabric.WebglFilterBackend();
	var canvas2dBackend = new fabric.Canvas2dFilterBackend()

	fabric.filterBackend = fabric.initFilterBackend();
	fabric.Object.prototype.transparentCorners = false;

	function applyFilter(index, filter) {
		var obj = App.canvas.getActiveObject();
		if(obj){
			obj.filters[index] = filter;
			var timeStart = +new Date();
			obj.applyFilters();
			var timeEnd = +new Date();
			var dimString = App.canvas.getActiveObject().width + ' x ' +
			App.canvas.getActiveObject().height;
			App.$id('bench').innerHTML = dimString + 'px ' +
			parseFloat(timeEnd-timeStart) + 'ms';
			App.canvas.renderAll();
		}
	}

	function getFilter(index) {
		var obj = App.canvas.getActiveObject();
		return obj.filters[index];
	}

	function applyFilterValue(index, prop, value) {
		var obj = App.canvas.getActiveObject();
		if (obj.filters[index]) {
			obj.filters[index][prop] = value;
			var timeStart = +new Date();
			obj.applyFilters();
			var timeEnd = +new Date();
			var dimString = App.canvas.getActiveObject().width + ' x ' +
			App.canvas.getActiveObject().height;
			App.$id('bench').innerHTML = dimString + 'px ' +
			parseFloat(timeEnd-timeStart) + 'ms';
			App.canvas.renderAll();
		}
	}

	// fabric.Object.prototype.padding = 5;
	// fabric.Object.prototype.transparentCorners = false;

	f = fabric.Image.filters;

	var indexF;
	App.$id('webgl').onclick = function() {
		if (this.checked) {
			fabric.filterBackend = webglBackend;
		} else {
			fabric.filterBackend = canvas2dBackend;
		}
	};
	App.$get_filter('brownie').onclick = function() {
		applyFilter(4, this.checked && new f.Brownie());
	};
	App.$get_filter('vintage').onclick = function() {
		applyFilter(9, this.checked && new f.Vintage());
	};
	App.$get_filter('technicolor').onclick = function() {
		applyFilter(14, this.checked && new f.Technicolor());
	};
	App.$get_filter('polaroid').onclick = function() {
		applyFilter(15, this.checked && new f.Polaroid());
	};
	App.$get_filter('kodachrome').onclick = function() {
		applyFilter(18, this.checked && new f.Kodachrome());
	};
	App.$get_filter('blackwhite').onclick = function() {
		applyFilter(19, this.checked && new f.BlackWhite());
	};
	App.$get_filter('grayscale').onclick = function() {
		$('[filter-op=grayscale]').slideToggle();
		applyFilter(0, this.checked && new f.Grayscale());
	};
	App.$get_filter('average').onclick = function() {
		applyFilterValue(0, 'mode', 'average');
	};
	App.$get_filter('luminosity').onclick = function() {
		applyFilterValue(0, 'mode', 'luminosity');
	};
	App.$get_filter('lightness').onclick = function() {
		applyFilterValue(0, 'mode', 'lightness');
	};
	App.$get_filter('invert').onclick = function() {
		applyFilter(1, this.checked && new f.Invert());
	};
	App.$get_filter('remove-color').onclick = function () {
		$('[filter-op=remove-color]').slideToggle();
		applyFilter(2, this.checked && new f.RemoveColor({
			distance: App.$get_filter('remove-color-distance').value,
			color: App.$get_filter('remove-color-color').value,
		}));
	};
	App.$get_filter('remove-color-color').onchange = function() {
		applyFilterValue(2, 'color', this.value);
	};
	App.$get_filter('remove-color-distance').oninput = function() {
		applyFilterValue(2, 'distance', this.value);
	};
	App.$get_filter('sepia').onclick = function() {
		applyFilter(3, this.checked && new f.Sepia());
	};
	App.$get_filter('brightness').onclick = function () {
		$('[filter-op=brightness]').slideToggle();
		applyFilter(5, this.checked && new f.Brightness({
			brightness: parseFloat(App.$get_filter('brightness-value').value)
		}));
	};
	App.$get_filter('brightness-value').oninput = function() {
		applyFilterValue(5, 'brightness', parseFloat(this.value));
	};
	App.$get_filter('gamma').onclick = function () {
		$('[filter-op=gamma]').slideToggle();
		var v1 = parseFloat(App.$get_filter('gamma-red').value);
		var v2 = parseFloat(App.$get_filter('gamma-green').value);
		var v3 = parseFloat(App.$get_filter('gamma-blue').value);
		applyFilter(17, this.checked && new f.Gamma({
			gamma: [v1, v2, v3]
		}));
	};
	App.$get_filter('gamma-red').oninput = function() {
		var current = getFilter(17).gamma;
		current[0] = parseFloat(this.value);
		applyFilterValue(17, 'gamma', current);
	};
	App.$get_filter('gamma-green').oninput = function() {
		var current = getFilter(17).gamma;
		current[1] = parseFloat(this.value);
		applyFilterValue(17, 'gamma', current);
	};
	App.$get_filter('gamma-blue').oninput = function() {
		var current = getFilter(17).gamma;
		current[2] = parseFloat(this.value);
		applyFilterValue(17, 'gamma', current);
	};
	App.$get_filter('contrast').onclick = function () {
		$('[filter-op=contrast]').slideToggle();
		applyFilter(6, this.checked && new f.Contrast({
			contrast: parseFloat(App.$get_filter('contrast-value').value)
		}));
	};
	App.$get_filter('contrast-value').oninput = function() {
		applyFilterValue(6, 'contrast', parseFloat(this.value));
	};
	App.$get_filter('saturation').onclick = function () {
		$('[filter-op=saturation]').slideToggle();
		applyFilter(7, this.checked && new f.Saturation({
			saturation: parseFloat(App.$get_filter('saturation-value').value)
		}));
	};
	App.$get_filter('saturation-value').oninput = function() {
		applyFilterValue(7, 'saturation', parseFloat(this.value));
	};
	App.$get_filter('noise').onclick = function () {
		$('[filter-op=noise]').slideToggle();
		applyFilter(8, this.checked && new f.Noise({
			noise: parseInt(App.$get_filter('noise-value').value, 10)
		}));
	};
	App.$get_filter('noise-value').oninput = function() {
		applyFilterValue(8, 'noise', parseInt(this.value, 10));
	};
	App.$get_filter('pixelate').onclick = function() {
		$('[filter-op=pixelate]').slideToggle();
		applyFilter(10, this.checked && new f.Pixelate({
			blocksize: parseInt(App.$get_filter('pixelate-value').value, 10)
		}));
	};
	App.$get_filter('pixelate-value').oninput = function() {
		applyFilterValue(10, 'blocksize', parseInt(this.value, 10));
	};
	App.$get_filter('blur').onclick = function() {
		$('[filter-op=blur]').slideToggle();
		applyFilter(11, this.checked && new f.Blur({
			value: parseFloat(App.$get_filter('blur-value').value)
		}));
	};
	App.$get_filter('blur-value').oninput = function() {
		applyFilterValue(11, 'blur', parseFloat(this.value, 10));
	};
	App.$get_filter('sharpen').onclick = function() {
		applyFilter(12, this.checked && new f.Convolute({
			matrix: [  0, -1,  0,
			-1,  5, -1,
			0, -1,  0 ]
		}));
	};
	App.$get_filter('emboss').onclick = function() {
		applyFilter(13, this.checked && new f.Convolute({
			matrix: [ 1,   1,  1,
			1, 0.7, -1,
			-1,  -1, -1 ]
		}));
	};
	App.$get_filter('blend').onclick= function() {
		$('[filter-op=blend]').slideToggle();
		applyFilter(16, this.checked && new f.BlendColor({
			color: App.$get_filter('blend-color').value,
			mode: App.$get_filter('blend-mode').value,
			alpha: App.$get_filter('blend-alpha').value
		}));
	};

	App.$get_filter('blend-mode').onchange = function() {
		applyFilterValue(16, 'mode', this.value);
	};

	App.$get_filter('blend-color').onchange = function() {
		applyFilterValue(16, 'color', this.value);
	};

	App.$get_filter('blend-alpha').oninput = function() {
		applyFilterValue(16, 'alpha', this.value);
	};

	App.$get_filter('hue').onclick= function() {
		$('[filter-op=hue]').slideToggle();
		applyFilter(21, this.checked && new f.HueRotation({
			rotation: App.$get_filter('hue-value').value,
		}));
	};

	App.$get_filter('hue-value').oninput = function() {
		applyFilterValue(21, 'rotation', this.value);
	};
};